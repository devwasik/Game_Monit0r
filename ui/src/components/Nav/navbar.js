import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Navbar extends Component {
  render() {
    return (
            <nav>
        <ul>
        
          <NavLink to='/'><div id="navLink"><li><span>Home</span></li></div></NavLink>
          <NavLink to='/'><div id="navLink"><li><span>Stats</span></li></div></NavLink>
          <NavLink to='/'><div id="navLink"><li><span>Games</span></li></div></NavLink>
          <NavLink to='/'><div id="navLink"><li><span>Friends</span></li></div></NavLink>
          <NavLink to='/'><div id="navLink"><li><span>Settings</span></li></div></NavLink>

        </ul>
      </nav>
    );
  }
}

export default Navbar;
