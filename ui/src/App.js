import React, { Component } from 'react';
import logo from './logo.png';
import Navbar from './components/Nav/navbar.js';
import GoogleLogin from 'react-google-login';
import './App.css';


const responseGoogle = (response) => {
  console.log(response);
}


class App extends Component {
  render() {
    return (
      <div className="App">
      <div className="col-sm-2 nav">
        <Navbar />
      </div>
      <div className="col-sm-10 content">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>Welcome back person</p>
          <div id="googleButton"></div>
        </header>
      </div>
      <GoogleLogin
    clientId="1443158415-v2mmk293luo3akmof80q79mbuvth10gf.apps.googleusercontent.com"
    buttonText="Login"
    onSuccess={responseGoogle}
    onFailure={responseGoogle}
  />,
  document.getElementById('googleButton')
      </div>
    );
  }
}

export default App;
