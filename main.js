const { app, BrowserWindow } = require('electron')
const url = require('url')
const path = require('path')
const notifier = require('node-notifier');

let win

function createWindow() {
    win = new BrowserWindow({ width: 800, height: 600 })
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))
}

const find = require('find-process');

find('name', 'wwwwww.net')
    .then(function(data) {
        console.log(data);
        let game = 'World of Warcraft';
        notifier.notify({
                title: 'GameTimeMonitor',
                message: 'Now playing ' + game,
                icon: path.join(__dirname, 'coulson.jpg'), // Absolute path (doesn't work on balloons)
                sound: true, // Only Notification Center or Windows Toasters
            },
            function(err, response) {
                // Response is response from notification``  l
            }
        );
    }, function(err) {
        console.log(err.stack || err);
    })



app.on('ready', createWindow)